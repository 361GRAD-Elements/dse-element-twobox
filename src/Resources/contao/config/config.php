<?php

/**
 * 361GRAD Element Two box
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_twobox'] =
    'Dse\\ElementsBundle\\ElementTwobox\\Element\\ContentDseTwobox';

// Cascading Style Sheets
$GLOBALS['TL_CSS']['dse_twobox'] = 'bundles/dseelementtwobox/css/fe_ce_dse_twobox.css';
