<?php

/**
 * 361GRAD Element Two box
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_twobox'] =
    '{type_legend},type,headline,dse_subheadline,dse_secondline;' .
    '{twobox_legend},singleSRC,dse_ctaHref,dse_ctaTitle;' .
    '{invisible_legend:hide},invisible,start,stop';

// Element fields
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondline'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'clr'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_subheadline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['singleSRC']['eval']['mandatory'] = false;


$GLOBALS['TL_DCA']['tl_content']['fields']['dse_ctaHref'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50 clr wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker'
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_ctaTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_ctaTitle'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50 ',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
