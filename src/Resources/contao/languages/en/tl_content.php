<?php

/**
 * 361GRAD Element Two box
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_twobox']   = ['2 Box', '2 Box'];

$GLOBALS['TL_LANG']['tl_content']['twobox_legend'] = '2 Box';

$GLOBALS['TL_LANG']['tl_content']['dse_secondline']  =
    ['Headline (Line 2)', 'Here you can add a second line to the headline.'];
$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'] =
    ['Subheadline', 'Here you can add a Subheadline.'];

$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref']  = [
    'CTA Link',
    'Here you can define a link to an intern/extern page.'
];
$GLOBALS['TL_LANG']['tl_content']['dse_ctaTitle'] = [
    'CTA Titel',
    'Here you can enter the CTA Button title.'
];